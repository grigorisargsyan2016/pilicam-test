export const state = () => ({
  vacancies: []
})

export const mutations = {
  setVacancies(state, vacancies) {
    state.vacancies = vacancies
  }
}

export const actions = {
  async getVacancies({ commit }) {
    const res = await this.$axios.$get("/api/positions.json?description=api")
    const third = Math.floor(res.length / 3 * 2)
    const vacancies = res.map((item, index) => {
      if (index > third) {
        return { ...item, type: "Part Time" }
      } else {
        return item
      }
    })
    commit("setVacancies", vacancies)
  }
}


export const getters = {
  vacancies: state => state.vacancies,
  vacanciesByType: state => {
    const partTimeVacancies = []
    const fullTimeVacancies = []
    const otherTypeVacancies = []
    for (const item of state.vacancies) {
      if (item.type === "Part Time") {
        partTimeVacancies.push(item)
      } else if (item.type === "Full Time") {
        fullTimeVacancies.push(item)
      } else {
        otherTypeVacancies.push(item)
      }
    }
    return {
      partTimeVacancies,
      fullTimeVacancies,
      otherTypeVacancies,
    }
  },
}
